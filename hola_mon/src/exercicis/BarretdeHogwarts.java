package exercicis;

import java.util.Scanner;

public class BarretdeHogwarts {
	
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		String a = sc.nextLine();

		if (a.equals("Coratge")) {

			System.out.println("Gryffindor");

		} else if (a.equals("Coneixement")){
			
			System.out.println("Ravenclaw");
			
		} else if (a.equals("Ambicio")) {
			
			System.out.println("Slytherin");
			
		} else {
			
			System.out.println("Hufflepuff");
			
		}
		
	}

}
