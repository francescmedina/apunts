import java.util.Scanner;

public class Exercici2FrancescMedina {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		// He creat un bucle per al cas de que l'usuari poses una lletra, es queda a
		// dins del bucle fins que l'ususari no posi un numero i he interrumput el bucle
		// infinit amb el sc.next()
		while (sc.hasNextInt() == false) {

			System.out.println("No es cap numero");

			sc.next();

		}

		int a = sc.nextInt();

		while (sc.hasNextInt() == false) {

			System.out.println("No es cap numero");

			sc.next();

		}
		// Declaracio de variables
		int b = sc.nextInt();

		int c;
		// Aqui he igualat i he fet el canvi de variables per a intercanviarlos
		c = a;

		a = b;

		b = c;
		// He decalarat una variable que es el resultat de la divisio
		int div = a / b;

		System.out.println("Divisio: " + div);
		// He decalarat una variable que es el resultat de la suma
		int sum = a + b;

		System.out.println("Suma: " + sum);
		// He decalarat una variable que es el resultat de la resta
		int res = a - b;

		System.out.println("Resta: " + res);
		// He decalarat una variable que es el resultat de la resta de la divisio
		int rdiv = a % b;

		System.out.println("Resta de la divisio: " + rdiv);
		// He decalarat una variable que es el resultat de la potencia
		int pot = (int) Math.pow(a, 2);

		System.out.println("Potencia: " + pot);
		// He decalarat una variable que es el resultat de la potencia
		int pot2 = (int) Math.pow(b, 2);

		System.out.println("Potencia de B: " + pot2);

	}

}
