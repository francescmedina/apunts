import java.util.Scanner;

public class Exercici1FrancescMedina {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		// Poso variables nom, any i curs

		System.out.println("Nom");
		String nom = sc.nextLine();

		System.out.println("Any");
		int aneixement = sc.nextInt();

		System.out.println("Curs");
		int curs = sc.nextInt();

		// Poso variables assignatures

		String a1, a2, a3, a4;

		System.out.println("Assignatura 1");
		a1 = sc.next();

		// Notes assignatura 1

		int n1, n2, n3, n4;

		System.out.println("Nota 1");
		n1 = sc.nextInt();

		System.out.println("Nota 2");
		n2 = sc.nextInt();

		System.out.println("Nota 3");
		n3 = sc.nextInt();

		System.out.println("Nota 4");
		n4 = sc.nextInt();

		// Nota mitja 1

		int nm = (n1 + n2 + n3 + n4) / 4;

		// Notes assignatura 2

		System.out.println("Assignatura 2");
		a2 = sc.next();

		int n5, n6, n7, n8;

		System.out.println("Nota 1");
		n5 = sc.nextInt();

		System.out.println("Nota 2");
		n6 = sc.nextInt();

		System.out.println("Nota 3");
		n7 = sc.nextInt();

		System.out.println("Nota 4");
		n8 = sc.nextInt();

		// Nota mitja 2

		int nm2 = (n5 + n6 + n7 + n8) / 4;

		// Notes assignatura 3

		System.out.println("Assignatura 3");
		a3 = sc.next();

		int n9, n10, n11, n12;

		System.out.println("Nota 1");
		n9 = sc.nextInt();

		System.out.println("Nota 2");
		n10 = sc.nextInt();

		System.out.println("Nota 3");
		n11 = sc.nextInt();

		System.out.println("Nota 4");
		n12 = sc.nextInt();

		// Nota mitja 3

		int nm3 = (n9 + n10 + n11 + n12) / 4;

		// Notes assignatura 4

		System.out.println("Assignatura 4");
		a4 = sc.next();

		int n13, n14, n15, n16;

		System.out.println("Nota 1");
		n13 = sc.nextInt();

		System.out.println("Nota 2");
		n14 = sc.nextInt();

		System.out.println("Nota 3");
		n15 = sc.nextInt();

		System.out.println("Nota 4");
		n16 = sc.nextInt();

		// Nota mitja 4

		int nm4 = (n13 + n14 + n15 + n16) / 4;

		// Resultats

		System.out.println("IES Sabadell " + curs);

		System.out.println(nom + ": " + (2020 - aneixement));

		System.out.println(a1 + ": " + n1 + ", " + n2 + ", " + n3 + ", " + n4 + ". La nota mitja es: " + nm);

		System.out.println(a2 + ": " + n5 + ", " + n6 + ", " + n7 + ", " + n8 + ". La nota mitja es: " + nm2);

		System.out.println(a3 + ": " + n9 + ", " + n10 + ", " + n11 + ", " + n12 + ". La nota mitja es: " + nm3);

		System.out.println(a4 + ": " + n13 + ", " + n14 + ", " + n15 + ", " + n16 + ". La nota mitja es: " + nm4);

	}

}
