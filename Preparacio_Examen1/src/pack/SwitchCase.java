package pack;

import java.util.Scanner;

public class SwitchCase {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		boolean exit = false;

		while (exit == false) {

			System.out.println("Q: Skill 1");
			
			System.out.println("W: Skill 2");
			
			System.out.println("E: Skill 3");
			
			System.out.println("R: Ulti");
			
			System.out.println("B: acabar programa");
			
			String tecla = sc.nextLine();
			
			tecla = tecla.toUpperCase();

			switch (tecla) {

			case ("Q"):

				System.out.println("Skill 1");
				break;
				
			case ("W"):

				System.out.println("Skill 2");
				break;

			case ("E"):

				System.out.println("Skill 3");

				break;

			case ("R"):

				System.out.println("Ulti");

				break;

			case ("B"):

				exit = true;

				break;

			default:

				System.out.println("No son aquestes tecles");

				break;

			}

		}

	}
}