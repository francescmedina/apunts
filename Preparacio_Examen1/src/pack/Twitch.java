package pack;

import java.util.Scanner;

public class Twitch {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int voltes = sc.nextInt();

		int hp = 0;

		int ra = 0;

		int tw = 0;

		while (voltes > 0) {

			while (hp <= 0) {

				System.out.println("Vida monstre de la Jungla:");
				hp = sc.nextInt();
			}

			while (ra <= 0) {
				System.out.println("Atac Rammus:");
				ra = sc.nextInt();
			}

			while (tw <= 0) {
				System.out.println("Atac Twitch:");
				tw = sc.nextInt();
			}

			if (hp > 0) {

				hp = hp - ra;

				if (hp <= 0) {

					System.out.println("RAMMUS");

				} else {

					hp = hp - tw;

					if (hp <= 0) {

						System.out.println("TWITCH");

					}

				}

			}

			hp=0;
			ra=0;
			tw=0;
			voltes = voltes - 1;
		}

	}

}
